<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin extends CI_Controller {
   
    
    public $Public_Vars = array();
    public $Sesson_Vars = array();
    public $Merged_Vars = array();

    public function __construct() {
        parent::__construct();
        if(!$this->session->userdata('auth')){
            redirect('login','refresh');
        }
        else{
            $this->Public_Vars = $this->property('title','BRTC-Service');
            $this->Sesson_Vars = $this->session->userdata('auth');
            if($this->Sesson_Vars['role'] != 1){
                redirect('login','refresh');
            }
            else{
                $this->Merged_Vars = array_merge($this->Public_Vars, $this->Sesson_Vars);
            }
        }
    }
    public function index(){
        $this->load->view('common/header',  $this->Merged_Vars);
        $this->load->view('common/sidebar',  $this->Merged_Vars);
        $this->load->view('common/footer',  $this->Merged_Vars);
        
    }
    public function any(){
        $this->load->view('common/header',  $this->Merged_Vars);
        $this->load->view('common/sidebar',  $this->Merged_Vars);
        $this->load->view('common/footer',  $this->Merged_Vars);
        
    }
}
?>