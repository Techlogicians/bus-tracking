<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class forgot extends CI_Controller{
    public $Public_Vars = array();
    public $Sesson_Vars = array();
    public $Merged_Vars = array();
    
       public function __construct() {
        parent::__construct();
        $this->Public_Vars = $this->property('title','ETS');
        $this->Merged_Data = $this->Public_Vars;
        if($this->session->userdata('auth')){
            $this->Sesson_Vars   = $this->session->userdata('auth');
            if($this->Sesson_Vars['role'] == 1){
                redirect('admin','refresh');
            }
            elseif($this->Sesson_Vars['role'] == 2){
               redirect('accounts','refresh');
            }
            elseif($this->Sesson_Vars['role'] == 3){
                redirect('assistant_accounts','refresh');

            }
            elseif($this->Sesson_Vars['role'] == 4){
                redirect('moderator','refresh');

            }
            elseif($this->Sesson_Vars['role'] == 5){
                redirect('clients','refresh');

            }
        }
    }
    
    public function index(){
        if(!$this->input->post('trigger')){
           $this->load->view('login/forgot', $this->Merged_Data);
        }
        else{
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|xss_clean');
               if($this->form_validation->run() === FALSE){
                $this->session->set_flashdata('errorData',  '<div class="alert alert-error"><strong>'.validation_errors().'</strong></div>');
                redirect('forgot');
            }
            else{
                $success = $this->recovermodel->checkMailValidity($this->input->post('email'));
                if($success != FALSE){
                    $this->sendEmail($this->input->post('email'));
                    $this->session->set_flashdata('errorData',  '<div class="alert alert-success"><strong>Success!</strong>A mail has been sent to your mail. Please follow the steps carefully.</div>');
                    redirect('forgot');
                }
                else{
                    $this->session->set_flashdata('errorData',  '<div class="alert alert-error"><strong>Failed!</strong>No Match Found!</div>');
                    redirect('forgot');
                }
            }
        }
   }
    public function sendEmail($umail){
        $email = $umail;
        $this->email->from('admin@ets.com', 'Support -ETS');
        $this->email->to($email);
        $this->email->subject('Password Recovery !!');
        $rendom=  $this->genRendom(30);
        $result= $this->recovermodel->getUserByMail($umail);
        foreach ($result as $row){
            $name = $row['u_name'];
            
        }
        $this->data=array('rend'=>md5($rendom),'name'=>$name);
        $msg =  $this->load->view('login/mailbody',  $this->data,TRUE);
        $this->email->message($msg);
        $mailSend=$this->email->send();
        if (!$mailSend) {
            show_error($this->email->print_debugger());
            
        }
        else {
            $data=array('u_email'=>$email,'rend'=>md5($rendom),'date'=> date('now'));
            $this->recovermodel->insertRen($data);
            return true;
            
        }
                
    }
    private function genRendom ($length = 50){
        $rendom= "";
        $possible = "0123456789abcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
            if (!strstr($rendom, $char)) {
                $rendom.= $char;
                $i++;
                
            }
            
        }
      return $rendom;
      
    }
 }
?>
