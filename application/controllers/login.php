<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends CI_Controller {
    
    public $Public_Vars = array();
    public $Sesson_Vars = array();
    public $Merged_Data = array();
            
    public function __construct() {
        parent::__construct();
        $this->Public_Vars = $this->property('title','BRTC-Service');
        $this->Merged_Data = $this->Public_Vars;
        if($this->session->userdata('auth')){
            $this->Sesson_Vars   = $this->session->userdata('auth');
            if($this->Sesson_Vars['role'] == 1){
                redirect('admin','refresh');
            }
            elseif($this->Sesson_Vars['role'] == 2){
               redirect('accounts','refresh');
            }
           
        }
    }

    public function index(){
        if($this->input->post()){
            $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'required|xss_clean');
            if($this->form_validation->run() === FALSE){
                $this->session->set_flashdata('errorData',  '<div class="alert alert-error"><strong>'.validation_errors().'</strong></div>');
                redirect('login');
            }
           
            else{
                $user_ip = $this->get_client_ip();
                $Score = $this->loginmodel->login($this->input->post('email'), $this->input->post('password'));
                if($Score != FALSE){
                    $now= time();
                    foreach ($Score->result() as $Score){
                        $this->session->set_userdata('auth', array('memb' => $Score->u_id,'name' => $Score->u_name,'pass' => $Score->u_pass,'mail' => $Score->u_email,'fone' => $Score->u_phone,'role' => $Score->ut_id,'time'=>gmdate('H:i:s', $now)));
                        $log_sess = $this->session->userdata('auth');
                        $log_data = array('date'=>date('Y m d'),'time'=>$log_sess['time'],'user'=>$log_sess['memb'],'u_role'=>$log_sess['role'],'ip_addr'=>$user_ip,'status'=>'Successful');
                        $this->loginmodel->log($log_data);
                        switch ($log_sess['role']){
                            case 1:
                            {
                                redirect('admin','refresh');
                                break;
                            }
                            case 2:
                            {
                                redirect('user','refresh');
                                break;
                            }
                           
                        }

                  }
                  
                }
                else{
                     $now= time();
                    $log_data = array('date'=>date('Y m d'),'time'=>gmdate('H:i:s', $now),'user'=>0,'u_role'=>0,'ip_addr'=>$user_ip,'status'=>'Failed');
                    $this->loginmodel->log($log_data);
                    $this->session->set_flashdata('errorData',  '<div class="alert alert-error"><strong>Incorrect Username or Password</strong></div>');
                    redirect('login');
                }
            }
        }
        else{
            $this->load->view('login/index', $this->Merged_Data);
        }
    }
    public  function get_client_ip() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
   
    } 
  
}