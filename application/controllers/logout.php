<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class logout extends CI_Controller {
    
    public $Public_Vars     = array();
    public $Session_Data    = array();
            
    public function __construct(){
        parent::__construct();
        $this->Public_Vars  = $this->property();
    }
    
    public function index()
    {
        $this->session->unset_userdata('auth');
        redirect(base_url(), 'refresh');
    }
}
?>
