<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class reset extends CI_Controller{
    public $Public_Vars = array();
    public $Sesson_Vars = array();
    public $Merged_Vars = array();
    
    public function __construct() {
        parent::__construct();
        $this->Public_Vars = $this->property('title','ETS');
        $this->Merged_Data = $this->Public_Vars;
        if($this->session->userdata('auth')){
            $this->Sesson_Vars   = $this->session->userdata('auth');
            if($this->Sesson_Vars['role'] == 1){
                redirect('admin','refresh');
            }
            elseif($this->Sesson_Vars['role'] == 2){
               redirect('accounts','refresh');
            }
            elseif($this->Sesson_Vars['role'] == 3){
                redirect('assistant_accounts','refresh');

            }
            elseif($this->Sesson_Vars['role'] == 4){
                redirect('moderator','refresh');

            }
            elseif($this->Sesson_Vars['role'] == 5){
                redirect('clients','refresh');

            }
        }
    }
    public function index($rend){
        if($this->recovermodel->checkRend($rend)){
        if($this->input->post('trigger')){
            $this->form_validation->set_rules('password', 'New Password', 'required|xss_clean');
            if($this->form_validation->run() === FALSE){
                $this->session->set_flashdata('errorData',  '<div class="alert alert-error"><strong>'.validation_errors().'</strong></div>');
                redirect('reset','refresh');
                
            }
            else{
                $result = $this->recovermodel-> getMailForReset($rend);
                foreach ($result as $row) {
                    $mail = $row['u_email'];
                    $u_id=$row['u_id'];
                    
                }
                $pass= $this->input->post('password');
                $reset = $this->recovermodel->editPassword($mail,$pass);
                if(!$reset){
                    $this->session->set_flashdata('errorData',  '<div class="alert alert-error"><strong>Error!</strong> You are trying to use an outdated or already used link.</div>');
                    redirect('reset');
                    
                }
                else{
                    $this->recovermodel->deleteRend($u_id);
                    $this->session->set_flashdata('errorData','<div class="alert alert-success"><button class="close" data-dismiss="alert">&times;</button><strong>Success!</strong> You may login Now!</div>');
                    redirect('login');
                    
                }
                    
            }
            
      }
      else {
          $this->data=array('rend'=>$rend);
          $this->load->view('login/newpassword', $this->Merged_Data,$this->data);
          
       }
     }
     else{
          $this->session->set_flashdata('errorData',  '<div class="alert alert-error"><strong>Error!</strong> You are trying to use an outdated or already used link.</div>');
          redirect('login');
      }   
    }
    public function reportspam($rend){
        if($this->recovermodel->checkRend($rend)){
         $result = $this->recovermodel-> getMailForReset($rend);
                foreach ($result as $row) {
                    $u_id=$row['u_id'];
                    
                }
                $this->recovermodel->deleteRend($u_id);
                $this->session->set_flashdata('errorData','<div class="alert alert-success"><button class="close" data-dismiss="alert">&times;</button><strong>Thank You!</strong> for your help!</div>');
                redirect('login');
        }
        else{
             $this->session->set_flashdata('errorData','<div class="alert alert-success"><button class="close" data-dismiss="alert">&times;</button><strong>Thank You!</strong> for your help!</div>');
                redirect('login');
        }
                
    }
   }

   ?>
