<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class service extends CI_Controller {
    public $Public_Vars = array();
    public $Sesson_Vars = array();
    public $Merged_Vars = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('servicemodel');
        if(!$this->session->userdata('auth')){
            redirect('login','refresh');
        }
        else{
            $this->Public_Vars = $this->property();
            $this->Sesson_Vars = $this->session->userdata('auth');
            if($this->Sesson_Vars['role'] != 1 ){
                redirect('login','refresh');
            }
            else{
                $this->Merged_Vars = array_merge($this->Public_Vars, $this->Sesson_Vars);
            }
        }
    }
  
    public function index(){
        $service  = $this->servicemodel->get_service();
        $i =0;
        foreach ($service->result() as $ser) {
            $sr_route[$i] = array('id'=>$ser->ser_Id,'ser'=>$ser->ser_name,'route'=>$this->servicemodel->get_service_route( $ser->ser_Id)->result()) ;
            $i++;
        }
        $this->Merged_Vars['sr_route'] = $sr_route;
        $this->load->view('common/header',  $this->Merged_Vars);
        $this->load->view('common/sidebar',  $this->Merged_Vars);
        $this->load->view('service/index',  $this->Merged_Vars);
        $this->load->view('common/footer',  $this->Merged_Vars);
          
    }
    public function delete($ser_id){
        $this->servicemodel->delete_ser($ser_id);
          $this->session->set_flashdata('serData',  '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Success!</b> Service deleted Successfully
                                    </div>');
          redirect(get_class($this));
    }
    public function edit($ser_id){
        if(!$this->input->post() && $ser_id != FALSE){
            $this->Merged_Vars['ser'] =  $this->servicemodel->get_service($ser_id);
            $this->load->view('common/header',  $this->Merged_Vars);
            $this->load->view('common/sidebar',  $this->Merged_Vars);
             $this->load->view('service/edit',  $this->Merged_Vars);
            $this->load->view('common/footer',  $this->Merged_Vars);
//            print_r($this->Merged_Vars['ser']->result());
        }
        
    }
    public function create( ){
        if(!$this->input->post()){
                $this->Merged_Vars['routes'] = $this->servicemodel->get_routes();
                $this->load->view('common/header',  $this->Merged_Vars);
                $this->load->view('common/sidebar',  $this->Merged_Vars);
                $this->load->view('service/create', $this->Merged_Vars);
                $this->load->view('common/footer',  $this->Merged_Vars);
        }
        else {
            $serdata=array( 'ser_name' =>$this->input->post('ser_name'));
          $ser_id =  $this->servicemodel->add($serdata);
          $routes = $this->input->post('route');
          $num_route = count($routes);
          for( $i = 0; $i < $num_route; $i++){
              $ser_route = array('ser_Id'=> $ser_id,'Route_Id'=>$routes[$i]);
              $this->servicemodel->add_service_route($ser_route);
          }
           $this->session->set_flashdata('serData',  '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Success!</b> Service added Successfully
                                    </div>');
          redirect(get_class($this));
        }
    
    }
}
?>