<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class recovermodel extends CI_Model{
    
    public function checkMailValidity($umail){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('u_email = "'.$umail.'"');
        $this->db->where('u_status = "1"');
        $result = $this->db->get();
        if($result->num_rows() == 1){
            return $result->result_array(); 
        }
        else {
            return false;
        }
    }
   
    public function editPassword($mail,$pass){
        $Data = array('u_pass' => md5($pass));
        $this->db->where('u_email = "'.$mail.'"');
        if($this->db->update('user', $Data)){
            return TRUE;
            
        }
        
    }
    public function insertRen($data){
       $this->db->where('u_email = "'.$data['u_email'].'"');
       $newdata = array('u_forgot_key'=>$data['rend'],'u_date'=>$data['date']);
        $this->db->update('user', $newdata);
        
    }
    public function getMailForReset($rend){
        $this->db->select('u_id,u_email,u_date');
        $this->db->from('user');
      $this->db->where('u_forgot_key = "'.$rend.'"');
        $result = $this->db->get();
        if($result->num_rows() == 1){
            return $result->result_array();
            
        }
        
    }
    public function checkRend($rend){
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('u_forgot_key = "'.$rend.'"');
        $this->db->where('u_date = "'.date('now').'"');
        $result = $this->db->get();
        if($result->num_rows() == 1){
            return true; 
        }
        else {
            return false;
        }
    }
    public function deleteRend($u_id){
       $this->db->where('u_id = "'.$u_id.'"');
       $newdata = array('u_forgot_key'=>"",'u_date'=>"");
        $this->db->update('user', $newdata);

    }
    public function getUserByMail($umail){
        $this->db->select('u_name');
        $this->db->from('user');
        $this->db->where('u_email = "'.$umail.'"');
        $result = $this->db->get();
        if($result->num_rows() == 1){
            return $result->result_array(); 
        }
        else {
            return false;
        }
        
    }
}
?>
