<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class servicemodel extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    
    public function get_service_route($ser_id){
        $this->db->select('service_route.id as id ,route.route_name as route');
        $this->db->from('service_route');
        $this->db->join('route', 'route.route_id  = service_route.route_id','left');
        $this->db->where('service_route.ser_id',$ser_id);
        $ser = $this->db->get();
        if($ser->num_rows()>0){
            return $ser;
        }
        return FALSE;
    }
    public function get_service($ser_id = FALSE){
        if($ser_id ==FALSE){
               $this->db->select('*');
               $ser = $this->db->get('service');
               return $ser;
        }
        else{
              $this->db->select('*');
                $ser = $this->db->get_where('service',array('ser_id'=>$ser_id));
                return $ser;
        }
     
    }
    
    public function delete_ser($ser_id){
        $this->db->delete('service_route' ,array('ser_id'=>$ser_id));
        $this->db->delete('service', array('ser_id'=>$ser_id));
    }
    public function get_routes(){
        $this->db->select('*');
        $routes = $this->db->get('route');
        return $routes;
    }
    public function add($serdata){
        $this->db->insert('service',$serdata);
        return $this->db->insert_id();
    }
    public function add_service_route($ser_route){
         $this->db->insert('service_route',$ser_route);
    }
}
?>
