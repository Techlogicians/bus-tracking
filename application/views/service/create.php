 <section class="content-header">
     <h1>
         General Form Elements
         <small>Preview</small>
     </h1>
     <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="#">service</a></li>
         <li class="active">create</li>
     </ol>
 </section>
<section class="content">
    <div class="row">
        <div class="col-md-8">
            <div class="box box-warning">
                <div class="box-header">
                    <h3 class="box-title">Service information</h3>
                </div>
                <div class="box-body">
                    <form role="form" method="post" action="">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text"  name="ser_name"class="form-control" placeholder="Enter service name" style="width: 70%;"/>
                        </div>
                        <div class="alert alert-info alert-dismissable">
                                        <i class="fa fa-info"></i><h4>Routes</h4>
                                        <?php  foreach ($routes->result() as $route){?>
                                        <label>
                                            <input type="checkbox" class="minimal-red"name="route[ ]" value="<?php echo $route->Route_Id;?> "/> <?php echo $route->Route_name;?> 
                                        </label><br>
                                            <?php }?>
                        </div>
                        <div class="box-footer">
                              <button type="submit" class="btn btn-primary">Submit</button>
                          </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>