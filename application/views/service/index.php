<section class="content-header">
    <h1>
        Service
        <small>Service Route</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo site_url('admin');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="<?php echo site_url('service') ?>">Service</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-10">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Service Route Table</h3>
                     <?php echo $this->session->flashdata('serData'); ?>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Service</th>
                            <th>Routes</th>
                            <th style="width: 200px">Action</th>
                        </tr>
                       <?php $sl= 1; foreach ($sr_route as $ser) {?>
                        <tr>
                          <td><?php echo $sl;?></td>
                                  <td class="center"><?php echo $ser['ser'];?></td>
                                  <td class="center">
                                      <?php $count =1 ; foreach ($ser['route']as $route) {?>
                                      <?php echo'('.$count.')'.$route->route;?><br>
                                        <?php $count++; } ?>
                                  </td>
                                  <td> 
                                      <div class="btn-group">
                                          <button type="button" class="btn btn-info">Action</button>
                                          <div class="btn-group">
                                              <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                                                  <span class="caret"></span>
                                              </button>
                                              <ul class="dropdown-menu">
                                                  <li><a href="<?php echo site_url('service/edit/'.$ser['id']);?> ">Edit</a></li>
                                                  <li><a onclick="return confirmation()" href="<?php echo site_url('service/delete/'.$ser['id']);?> ">Delete</a></li>
                                              </ul>
                                          </div>
                                      </div>
                                  </td>
                        </tr>
                        <?php $sl++; }?>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            </div>
        </div>
    </section>
<script>
function confirmation(){
   var retVal = confirm("Do you want to delete the service ?");
   if( retVal == true ){
       return true;
   }
   else{
            return false;
   }
}
</script>